#include <stdio.h>
#include <string.h>

typedef enum {
   err_string_buf_ovf,
   err_unexpected_tok,
   err_unexpected_eof
} errors;

#define OBJ       0xFF
#define ieq(c)    *p->cp == c
#define ile(c)    *p->cp <= c
#define ige(c)    *p->cp >= c
#define isws      ige(  1) && ile(' ')
#define iscr      ieq(':') || ieq(';') || ieq(',') || ieq('.') || ieq('[') || ieq(']') || ieq('(') || ieq(')')
#define isid      ige('!') && ile('~') && !(iscr) && !(ieq('"')) && !(ieq('#'))
#define clrsb()   *(--p->sp) = 0;
#define peeksym() (symbol *) p->tail->d;

typedef struct _node {
   void         *d;
   struct _node *x, *y;
} node;

typedef struct _symbol {
   int    id;
   char  *tx;
} symbol;

typedef struct _parser {
   FILE *fp;
   char *fn;
   int   ln;
   
   char cb[256], sb[256];
   char *cp, *sp;
   
   node *table, *head, *tail;
} parser;

void     error          (parser *p, int id);
void     debugsymbols   (node *n);
void     expect         (parser *p, int id);
symbol  *findsym        (node *t, int id, char *tx);
void     rdch           (parser *p);
void     rdtk           (parser *p);
void     obj            (parser *p);
parser  *parse          (char *fn);