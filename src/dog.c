#include "dog.h"

void error(parser *p, int id) {
   symbol *s = peeksym();
   
   printf("* error: file '%s', line %d, ", p->fn, p->ln);
   switch (id) {
      case err_string_buf_ovf: printf("string buffer overflowed."); break;
      case err_unexpected_tok: printf("unexpected symbol 0x%02X, '%s'", s->id, s->tx); break;
      case err_unexpected_eof: printf("unexpected end of file."); break;
   }
   printf("\n");
   exit(0);
}

void debugsymbols(node *n) { symbol *s; while (n) { s = (symbol *) n->d; printf("0x%08X: %02X '%s'\n", s, s->id, s->tx); n = n->x; } }
int  accept(parser *p, int id) { symbol *s = peeksym(); if (s->id == id) { rdtk(p); return 1; } return 0; }
void expect(parser *p, int id) { if (!accept(p, id)) { error(p, err_unexpected_tok); } }

symbol *findsym(node *t, int id, char *tx) {
   node *n = t;
   symbol *s;
   
   while (n) {
      s = (symbol *) n->d;
      if ((strcmp(s->tx, tx) == 0) && (s->id == id)) { return s; }
      n = n->x;
   }
   return NULL;
}

void rdch(parser *p) {
   int k = ((++p->cp) - p->cb) & 0xFF;
   
   if (p->fp) {
      if (k == 0) {
         k = fread(p->cb, 1, 256, p->fp);
         printf("* file '%s', read in %d bytes", p->fn, k);
         if (k < 256) {
            printf(", and is now completely loaded", p->fn);
            fclose(p->fp);
            p->fp = NULL;
            p->cb[k] = 0;
         }
         printf(".\n");
         p->cp = p->cb;
      }
   }
   
   if (*p->cp == '\n') { p->ln++; }
   
   if (p->sp == p->sb+254) { error(p, err_string_buf_ovf); return; }
   *(p->sp++) = *p->cp;
   *p->sp = 0;
}

void rdtk(parser *p) {
   int id = 0;
   char tmp;
   
   // clear any whitespace
   if (isws) { clrsb(); rdch(p); while (isws) { clrsb(); if (ieq(0)) { break; } rdch(p); } rdtk(p); return; }
   
   // clear any comments
   if (ieq('#')) {
      clrsb(); rdch(p);
      // clear multi-line whitespace
      if (ieq('*')) { clrsb(); rdch(p); while (1) { if (ieq(0)) { id = 0; break; } else if (ieq('*')) { clrsb(); rdch(p); if (ieq('#')) { clrsb(); rdch(p); break; } } clrsb(); rdch(p); } rdtk(p); return; }
      // clear single-line whitespace
      clrsb(); rdch(p); while (1) { if (ieq(0)) { id = 0; break; } else if (ieq('\n')) { clrsb(); rdch(p); break; } rdch(p); clrsb(); } rdtk(p); return;
   }
   
   // check control symbols
   if      (iscr    ) { id = *p->cp; rdch(p); }
   // check for objects
   else if (isid    ) { while (isid) { rdch(p); } id = OBJ; }
   // check for strings
   else if (ieq('"')) { clrsb(); rdch(p); while (1) { if (ieq(0) || ieq('"')) { break; } rdch(p); } clrsb(); rdch(p); id = OBJ; }

   // we have an extra byte in the string buffer, store it temporarily and remove it from the string buffer
   tmp = *(--p->sp);
   *p->sp = 0;
   
   // check if symbol is in symbol table
   symbol *s = findsym(p->table, id, p->sb);
   
   // if not, generate a new symbol and push it to the symbol table
   if (!s) {
      // create new symbol
      s = (symbol *) malloc(sizeof(symbol));
      s->tx = (char *) malloc(p->sp - p->sb);
      s->id = id;
      strcpy(s->tx, p->sb);
      
      // push symbol to symbol table
      node *n = (node *) malloc(sizeof(node));
      n->d = s;
      n->x = p->table;
      p->table = n;
   }
   
   // push symbol to code listing
   node *n = (node *) malloc(sizeof(node));
   n->d = s;
   n->x = NULL;
   
   if (p->tail) { p->tail->x = n; } // if the tail exists, connect it to the new node
   else         { p->head = n;    } // otherwise, this is the first node
   p->tail = n;                     // set this new node as the new tail
      
   // clear string buffer and add new character
   p->sp = p->sb;
   *p->sp = tmp;
   *(++p->sp) = 0;
}

void obj(parser *p) {
   int job = 0;
   symbol *s = peeksym();
   
   if (accept(p, '.')) {
      printf("self reference\n");
      obj(p);
   }
   else if (accept(p, '(')) {
      printf("begin tuple\n");
      obj(p);
      while (accept(p, ',')) {
         printf("comma\n");
         obj(p);
      }
      expect(p, ')');
      printf("end tuple\n");
   }
   else if (accept(p, '[')) {
      printf("begin array\n");
      obj(p);
      while (accept(p, ',')) {
         printf("comma\n");
         obj(p);
      }
      expect(p, ']');
      printf("end array\n");
   }
   else if (accept(p, OBJ)) {
      printf("object '%s'\n", s->tx);
      
      while (1) {
         if (accept(p, '(')) {
            printf("begin parameters\n");
            obj(p);
            while (accept(p, ',')) {
               printf("comma\n");
               obj(p);
            }
            expect(p, ')');
            printf("end parameters\n");
            job = 1;
         }
         else if (accept(p, '[')) {
            printf("begin index\n");
            obj(p);
            while (accept(p, ',')) {
               obj(p);
            }
            expect(p, ']');
            printf("end index\n");
            job = 2;
         }
         else {
            break;
         }
      }
      
      if (accept(p, '.')) {
         switch (job) {
            case 1: printf("invoke method\n"); break;
            case 2: printf("access array\n"); break;
         }
         printf("member reference\n");
         obj(p);
      } else if (accept(p, ':')) {
         printf("begin definition '%s'\n", s->tx); 
         while (1) {
            if (accept(p, ';')) { printf("end definition '%s'\n", s->tx);   break; }
            if (accept(p,   0)) { error(p, err_unexpected_eof); break; }
            obj(p);
         }
      } else {
         switch (job) {
            case 1: printf("invoke method\n"); break;
            case 2: printf("access array\n"); break;
         }
      }
   }
}

parser *parse(char *fn) {
   symbol *s;
   FILE   *fp;
   
   fp = fopen(fn, "r");
   if (fp) { printf("* file '%s' opened successfully for parsing.\n", fn); }
   else    { printf("* file '%s' failed to open for parsing.\n", fn); return NULL; }
   
   parser *p = (parser *) malloc(sizeof(parser));
   p->fp = fp; p->fn = fn; p->ln =  1;
   p->cp = p->cb - 1; p->sp = p->sb;
   p->table = NULL; p->head = NULL; p->tail = NULL;
   
   rdch(p);
   rdtk(p);
   
   printf("\n");
   while (1) {
      s = peeksym();
      if (s) { if (s->id == 0) { break; } } else { break; }
      obj(p);
   }
   printf("\nsymbol table:\n"); debugsymbols(p->table);
   printf("\ncode listing:\n"); debugsymbols(p->head);
}

int main() {
   parser *p = parse("test.dog");
   return 0;
}
