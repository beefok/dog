# README #

DOG: Data Object Graph, Compiler Toolkit

### What is this repository for? ###

* A tiny language called dog, or data object graph, allows for object oriented approach to data.
* Version 0.01

Examples:

This describes an object called bob who has 2 members and 1 method, and the method is also defined.
```
   bob:
      x:0;
      y:0;
      
      move(x, y):
         .x: add(.x, x) ;
         .y: add(.y, y) ;
      ;
   ;
```

```
   bob.dir:0;
   
   bob.turn(x):
      .dir: add(.dir, x) ;
   ;
```

'bob' now has a 'dir' member and a new method called 'turn' which affects the 'dir' member.
As you can see, this is a prototype-based object oriented language.
There are no operators 'per-se', everything is based on object message passing.

### How do I get set up? ###

* I just use tcc to compile it for now, tcc.exe -o dog.c dog.exe